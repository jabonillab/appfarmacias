package com.example.jorgebonilla.jbfarmacia.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.example.jorgebonilla.jbfarmacia.R;
import com.example.jorgebonilla.jbfarmacia.ServiceProvider;
import com.example.jorgebonilla.jbfarmacia.adapters.Producto2Adapter;
import com.example.jorgebonilla.jbfarmacia.dtos.PedidoDTO;
import com.example.jorgebonilla.jbfarmacia.dtos.ProductoDTO;

import static com.example.jorgebonilla.jbfarmacia.Factory.modelped;
import static com.example.jorgebonilla.jbfarmacia.Factory.modelprods;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Pedido");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> guardarPedido(modelped));

        ListView lv = findViewById(R.id.listView);
        ArrayList<ProductoDTO> productos = new ArrayList<>(modelprods.values());
        Producto2Adapter adapter = new Producto2Adapter(this, productos);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("¿Quieres eliminar este producto de la cesta?")
                    .setPositiveButton("Aceptar", (dialog, id1) -> {
                        ProductoDTO producto = productos.get(position);
                        modelped.getProductos().remove(producto.getStrid());
                        modelprods.remove(producto.getStrid());
                        Intent OrderActivity = new Intent(getApplicationContext(), OrderActivity.class);
                        startActivity(OrderActivity);
                        String texto = producto.getNombre() + " eliminado de la cesta";
                        Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_SHORT).show();
                    })
                    .setNegativeButton("Cancelar", (dialog, id12) -> {
                        // User cancelled the dialog
                    });
            AlertDialog alert = builder.create();
            alert.show();
        });
    }

    public void guardarPedido(PedidoDTO pdto) {
        Call<PedidoDTO> call = ServiceProvider.service.postOrder(pdto);
        call.enqueue(new Callback<PedidoDTO>() {
            @Override
            public void onResponse(@NonNull Call<PedidoDTO> call, @NonNull Response<PedidoDTO> response) {
                if (response.code() == 200) {
                    modelped = new PedidoDTO();
                    Intent MainActivity = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(MainActivity);
                    Toast.makeText(getApplicationContext(), "Compra realizada", Toast.LENGTH_SHORT).show();
                    if (response.body() != null) {
                        Log.d("MK", response.body().toString());
                    }
                } else {
                    Log.d("MK", response.toString());
                }

            }

            @Override
            public void onFailure(@NonNull Call<PedidoDTO> call, @NonNull Throwable t) {
            }
        });
    }
}