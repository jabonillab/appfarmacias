package com.example.jorgebonilla.jbfarmacia.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.jorgebonilla.jbfarmacia.Factory;
import com.example.jorgebonilla.jbfarmacia.R;
import com.example.jorgebonilla.jbfarmacia.ServiceProvider;
import com.example.jorgebonilla.jbfarmacia.adapters.ProductoAdapter;
import com.example.jorgebonilla.jbfarmacia.dtos.ProductoDTO;

import static com.example.jorgebonilla.jbfarmacia.Factory.modelfarm;
import static com.example.jorgebonilla.jbfarmacia.Factory.modelped;
import static com.example.jorgebonilla.jbfarmacia.Factory.modelprods;

import java.util.ArrayList;
import java.util.Objects;

public class ProductsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Productos");

        ListView lv = findViewById(R.id.listView);
        ArrayList<ProductoDTO> productos = new ArrayList<>(modelfarm.getProductos().values());
        ProductoAdapter adapter = new ProductoAdapter(this, productos);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            ProductoDTO producto = productos.get(position);
            Integer cantidad = modelped.getProductos().get(producto.getStrid());
            if (cantidad != null) {
                modelped.getProductos().put(producto.getStrid(), cantidad + 1);
            } else {
                modelprods.put(producto.getStrid(), producto);
                modelped.getProductos().put(producto.getStrid(), 1);
            }
            String texto = producto.getNombre() + " añadido a la cesta";
            Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.mybutton) {
            Intent OrderActivity = new Intent(getApplicationContext(), OrderActivity.class);
            startActivity(OrderActivity);
        }
        if (id == R.id.cerrar) {
            ServiceProvider.setService();
            Factory.reset();
            Intent LoginActivity = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(LoginActivity);
        }
        return super.onOptionsItemSelected(item);
    }
}