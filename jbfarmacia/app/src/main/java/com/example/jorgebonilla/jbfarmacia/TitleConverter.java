package com.example.jorgebonilla.jbfarmacia;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class TitleConverter extends AsyncTask<String, Void, Drawable> {
    @Override
    protected Drawable doInBackground(String... strings) {
        try {
            String url = "https://ui-avatars.com/api/?name=" + strings[0] + "&size=100";
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap x = BitmapFactory.decodeStream(input);
            return new BitmapDrawable(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}