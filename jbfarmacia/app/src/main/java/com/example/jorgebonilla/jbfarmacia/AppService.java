package com.example.jorgebonilla.jbfarmacia;

import com.example.jorgebonilla.jbfarmacia.dtos.FarmaciaDTO;
import com.example.jorgebonilla.jbfarmacia.dtos.PedidoDTO;
import com.example.jorgebonilla.jbfarmacia.dtos.UsuarioDTO;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AppService {

    @GET("/farmacias")
    Call<List<FarmaciaDTO>> getAllPharms();

    @POST("/farmacia")
    Call<ResponseBody> postPharm(@Body FarmaciaDTO fdto);

    @POST("/farmacia/eliminar")
    Call<ResponseBody> deletePharm(@Body FarmaciaDTO fdto);

    @POST("/pedido")
    Call<PedidoDTO> postOrder(@Body PedidoDTO pdto);

    @POST("/usuario")
    Call<ResponseBody> postUser(@Body UsuarioDTO usuarioDTO);

    @GET("/usuario")
    Call<UsuarioDTO> login();
}
