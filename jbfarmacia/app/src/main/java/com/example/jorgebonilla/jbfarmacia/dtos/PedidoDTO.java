/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jorgebonilla.jbfarmacia.dtos;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Felipe
 */
public class PedidoDTO {

    private String strid, nombre, comprador;
    private Date fechaDePedido;
    private Map<String, Integer> productos;
    private float precioTotal;

    public PedidoDTO() {
        strid = UUID.randomUUID().toString();
        nombre = "";
        comprador = "";
        fechaDePedido = new Date();
        productos = new LinkedHashMap<>();
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the productos
     */
    public Map<String, Integer> getProductos() {
        return productos;
    }

    /**
     * @param productos the productos to set
     */
    public void setProductos(Map<String, Integer> productos) {
        this.productos = productos;
    }

    /**
     * @return the strid
     */
    public String getStrid() {
        return strid;
    }

    /**
     * @param strid the strid to set
     */
    public void setStrid(String strid) {
        this.strid = strid;
    }

    /**
     * @return the precioTotal
     */
    public float getPrecioTotal() {
        return precioTotal;
    }

    /**
     * @param precioTotal the precioTotal to set
     */
    public void setPrecioTotal(float precioTotal) {
        this.precioTotal = precioTotal;
    }

    /**
     * @return the fechaDePedido
     */
    public Date getFechaDePedido() {
        return fechaDePedido;
    }

    /**
     * @param fechaDePedido the fechaDePedido to set
     */
    public void setFechaDePedido(Date fechaDePedido) {
        this.fechaDePedido = fechaDePedido;
    }

    /**
     * @return the comprador
     */
    public String getComprador() {
        return comprador;
    }

    /**
     * @param comprador the comprador to set
     */
    public void setComprador(String comprador) {
        this.comprador = comprador;
    }
}
