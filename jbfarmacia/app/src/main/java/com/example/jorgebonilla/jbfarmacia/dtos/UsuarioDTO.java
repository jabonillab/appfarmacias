/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jorgebonilla.jbfarmacia.dtos;

/**
 * @author Felipe
 */
public class UsuarioDTO {

    private String nick, contrasena, email;
    private boolean administrador;

    public UsuarioDTO() {
        nick = "";
        contrasena = "";
        email = "";
        administrador = false;
    }

    public UsuarioDTO(String nick) {
        this.nick = nick;
        email = "";
        administrador = false;
    }

    public UsuarioDTO(String nick, boolean administrador) {
        this.nick = nick;
        email = "";
        this.administrador = administrador;
    }

    public UsuarioDTO(String nick, String contrasena) {
        this.nick = nick;
        this.contrasena = contrasena;
        email = "";
        administrador = false;
    }

    /**
     * @return the nick
     */
    public String getNick() {
        return nick;
    }

    /**
     * @param nick the nick to set
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * @return the contrasena
     */
    public String getContrasena() {
        return contrasena;
    }

    /**
     * @param contrasena the contrasena to set
     */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * @return the administrador
     */
    public boolean isAdministrador() {
        return administrador;
    }

    /**
     * @param administrador the administrador to set
     */
    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}