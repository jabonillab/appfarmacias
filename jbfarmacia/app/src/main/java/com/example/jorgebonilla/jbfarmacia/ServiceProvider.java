package com.example.jorgebonilla.jbfarmacia;

import android.text.TextUtils;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ServiceProvider {

    private static final String API_BASE_URL = "https://dss-servidor-fpeiro.herokuapp.com/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(JacksonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public static AppService service;

    public ServiceProvider(){
        setService();
    }

    public static void setService() {
        setService(null, null);
    }

    public static void setService(String username, String password) {
        if (!TextUtils.isEmpty(username)
                && !TextUtils.isEmpty(password)) {
            String authToken = Credentials.basic(username, password);
            setService(authToken);
        }

        setService(null);
    }

    private static void setService(final String authToken) {
        if (!TextUtils.isEmpty(authToken)) {
            Interceptor interceptor = chain -> {
                Request original = chain.request();

                Request.Builder builder = original.newBuilder()
                        .header("Authorization", authToken);

                Request request = builder.build();
                return chain.proceed(request);
            };

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);

                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.addInterceptor(loggingInterceptor);

                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }

        service = retrofit.create(AppService.class);
    }
}
