package com.example.jorgebonilla.jbfarmacia.adapters;

import com.example.jorgebonilla.jbfarmacia.GMapsConverter;
import com.example.jorgebonilla.jbfarmacia.R;
import com.example.jorgebonilla.jbfarmacia.TitleConverter;
import com.example.jorgebonilla.jbfarmacia.dtos.FarmaciaDTO;

import static android.support.constraint.Constraints.TAG;
import static com.example.jorgebonilla.jbfarmacia.GMapsConverter.getAddress;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class FarmaciaAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<FarmaciaDTO> items;

    public FarmaciaAdapter(Activity activity, ArrayList<FarmaciaDTO> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_farmacia, parent, false);
        }

        FarmaciaDTO dir = items.get(position);

        TextView nombre = v.findViewById(R.id.nombre);
        nombre.setText(dir.getNombre());

        try {
            TextView ubicacion = v.findViewById(R.id.ubicacion);
            AsyncTask<Float, Void, String> address = new GMapsConverter().execute(dir.getLatitud(), dir.getLongitud());
            ubicacion.setText(address.get());
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        try {
            ImageView imagen = v.findViewById(R.id.imagen);
            AsyncTask<String, Void, Drawable> image = new TitleConverter().execute(dir.getNombre());
            imagen.setImageDrawable(image.get());
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        return v;
    }
}