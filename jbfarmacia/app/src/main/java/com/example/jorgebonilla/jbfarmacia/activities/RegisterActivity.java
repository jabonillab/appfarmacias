package com.example.jorgebonilla.jbfarmacia.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.example.jorgebonilla.jbfarmacia.R;
import com.example.jorgebonilla.jbfarmacia.ServiceProvider;
import com.example.jorgebonilla.jbfarmacia.dtos.UsuarioDTO;

import java.io.IOException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    AutoCompleteTextView mEmailView;
    EditText mPasswordView;
    View mProgressView;
    View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Pharmacist shop");

        Button mEmailSignInButton = findViewById(R.id.register2);
        mPasswordView = findViewById(R.id.password);
        mEmailView = findViewById(R.id.email);
        mLoginFormView = findViewById(R.id.register_form);
        mProgressView = findViewById(R.id.login_progress);


        mEmailSignInButton.setOnClickListener(view -> attemptRegister());


    }

    private void attemptRegister() {
        ServiceProvider.setService();

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        UsuarioDTO usuarioDTO = new UsuarioDTO(email, password);
        Call<ResponseBody> call = ServiceProvider.service.postUser(usuarioDTO);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.d("MK2", String.valueOf(response.code()));
                if (response.code() == 200) {
                    Intent LoginActivity = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(LoginActivity);
                    Log.d("Bien", "aca");

                    try {
                        if (response.body() != null) {
                            Log.d("MK2", response.body().string());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                }


            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

            }


        });


    }

}
