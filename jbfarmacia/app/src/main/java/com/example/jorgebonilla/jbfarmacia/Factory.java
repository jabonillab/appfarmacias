package com.example.jorgebonilla.jbfarmacia;

import com.example.jorgebonilla.jbfarmacia.dtos.FarmaciaDTO;
import com.example.jorgebonilla.jbfarmacia.dtos.PedidoDTO;
import com.example.jorgebonilla.jbfarmacia.dtos.ProductoDTO;

import java.util.LinkedHashMap;
import java.util.Map;

public class Factory {
    public static FarmaciaDTO modelfarm;
    public static PedidoDTO modelped;
    public static ProductoDTO modelprod;
    public static Map<String, FarmaciaDTO> modelfarms;
    public static Map<String, ProductoDTO> modelprods;

    public Factory() {
        reset();
    }

    public static void reset() {
        modelfarm = new FarmaciaDTO();
        modelped = new PedidoDTO();
        modelprod = new ProductoDTO();
        modelfarms = new LinkedHashMap<>();
        modelprods = new LinkedHashMap<>();
    }
}