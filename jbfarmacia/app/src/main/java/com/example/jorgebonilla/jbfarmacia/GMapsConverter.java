
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jorgebonilla.jbfarmacia;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.Normalizer;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Felipe
 **/
public class GMapsConverter extends AsyncTask<Float, Void, String> {

    public static Entry<Float, Float> getCoordinates(String address) throws IOException, NoSuchAlgorithmException, KeyManagementException, JSONException {
        String uri = "https://maps.google.com/maps/api/geocode/json?address="
                + address.replaceAll(" ", "%20")
                + "&key=AIzaSyACNa98lD-5NTSTjQCMOeWvYTAjanyMl78";

        JSONObject json = readJsonFromUrl(Normalizer.normalize(uri, Normalizer.Form.NFD).replaceAll("\\p{M}", ""));
        double lat = json.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lat");
        double lng = json.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lng");

        return new SimpleEntry<>((float) lat, (float) lng);
    }

    public static String getAddress(float lat, float lng) throws IOException, NoSuchAlgorithmException, KeyManagementException, JSONException {
        String uri = "https://maps.google.com/maps/api/geocode/json?latlng="
                + lat + "," + lng
                + "&key=AIzaSyACNa98lD-5NTSTjQCMOeWvYTAjanyMl78";

        JSONObject json = readJsonFromUrl(Normalizer.normalize(uri, Normalizer.Form.NFD).replaceAll("\\p{M}", ""));
        return json.getJSONArray("results").getJSONObject(0).getString("formatted_address");
    }

    private static JSONObject readJsonFromUrl(String uri) throws IOException, NoSuchAlgorithmException, KeyManagementException, JSONException {
        try (InputStream is = openStream(uri)) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            return new JSONObject(jsonText);
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    private static InputStream openStream(String uri) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkClientTrusted(final X509Certificate[] chain, final String authType) {
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkServerTrusted(final X509Certificate[] chain, final String authType) {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        }};

        final SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier((String string, SSLSession ssls) -> true);
        final URL url = new URL(uri);
        URLConnection connection = url.openConnection();
        return (InputStream) connection.getContent();
    }

    @Override
    protected String doInBackground(Float... floats) {
        try {
            return getAddress(floats[0], floats[1]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

