package com.example.jorgebonilla.jbfarmacia.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.jorgebonilla.jbfarmacia.Factory;
import com.example.jorgebonilla.jbfarmacia.R;
import com.example.jorgebonilla.jbfarmacia.ServiceProvider;
import com.example.jorgebonilla.jbfarmacia.adapters.FarmaciaAdapter;
import com.example.jorgebonilla.jbfarmacia.dtos.FarmaciaDTO;

import static com.example.jorgebonilla.jbfarmacia.Factory.modelfarm;
import static com.example.jorgebonilla.jbfarmacia.Factory.modelfarms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Farmacias");

        ListView lv = findViewById(R.id.listView);
        ArrayList<FarmaciaDTO> farmacias = new ArrayList<>(modelfarms.values());
        FarmaciaAdapter adapter = new FarmaciaAdapter(this, farmacias);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            modelfarm = farmacias.get(position);
            Intent ProductsActivity = new Intent(getApplicationContext(), ProductsActivity.class);
            startActivity(ProductsActivity);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.mybutton) {
            Intent OrderActivity = new Intent(getApplicationContext(), OrderActivity.class);
            startActivity(OrderActivity);
        }
        if (id == R.id.cerrar) {
            ServiceProvider.setService();
            Factory.reset();
            Intent LoginActivity = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(LoginActivity);
        }
        return super.onOptionsItemSelected(item);
    }

    public void guardarFarmacia(FarmaciaDTO fdto) {
        Call<ResponseBody> call = ServiceProvider.service.postPharm(fdto);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        //Acci�n
                        if (response.body() != null) {
                            Log.d("MK", response.body().string());
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            }
        });
    }

    public void eliminarFarmacia(FarmaciaDTO fdto) {
        Call<ResponseBody> call = ServiceProvider.service.deletePharm(fdto);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        //Acci�n
                        if (response.body() != null) {
                            Log.d("MK", response.body().string());
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            }
        });
    }
}
