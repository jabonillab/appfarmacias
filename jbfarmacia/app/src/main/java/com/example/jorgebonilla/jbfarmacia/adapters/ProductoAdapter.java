package com.example.jorgebonilla.jbfarmacia.adapters;

import com.example.jorgebonilla.jbfarmacia.R;
import com.example.jorgebonilla.jbfarmacia.dtos.ProductoDTO;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class ProductoAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<ProductoDTO> items;

    public ProductoAdapter(Activity activity, ArrayList<ProductoDTO> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_farmacia, parent, false);
        }

        ProductoDTO dir = items.get(position);

        TextView nombre = v.findViewById(R.id.nombre);
        nombre.setText(dir.getNombre());

        TextView precio = v.findViewById(R.id.ubicacion);
        String texto = dir.getPrecio() + " €";
        precio.setText(texto);

        if (!dir.getImagen().equals("")) {
            ImageView imagen = v.findViewById(R.id.imagen);

            byte[] bytes = dir.getImagen().getBytes();
            byte[] decodeBase64 = Base64.decode(bytes, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(decodeBase64, 0, decodeBase64.length);
            Drawable image = new BitmapDrawable(bitmap);

            imagen.setImageDrawable(image);
        }

        return v;
    }
}